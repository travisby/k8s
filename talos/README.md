# Commands

## Reset

 ```sh
talosctl reset \
  --graceful=false \
  --reboot \
  --system-labels-to-wipe STATE \
  --system-labels-to-wipe EPHEMERAL \
  -n $node \
  -e $node
```

## Generate configs

```sh
# talosctl gen secrets
./script.sh
```

## Apply Configs

Get IPs:

```sh
tpi uart get -n 1 | grep 'assigned address'
tpi uart get -n 2 | grep 'assigned address'
tpi uart get -n 3 | grep 'assigned address'
tpi uart get -n 4 | grep 'assigned address'
```

```sh
talosctl apply-config --insecure -n $IP1 -f ./controlplane.yaml -p @patches/per-host/controlplane/rk101.yaml
talosctl apply-config --insecure -n $IP2 -f ./controlplane.yaml -p @patches/per-host/controlplane/rk102.yaml
talosctl apply-config --insecure -n $IP3 -f ./controlplane.yaml -p @patches/per-host/controlplane/rk103.yaml
talosctl apply-config --insecure -n $IP4 -f ./controlplane.yaml -p @patches/per-host/controlplane/rk104.yaml
```

Usually the IP addresses change during this :eyeroll:

```sh
tpi uart get -n 1 | grep 'assigned address'
```

```sh
talosctl bootstrap -n $NEWIP1 -e $NEWIP1
```
