CLUSTER_NAME=turingpi
CLUSTER_ENDPOINT=https://192.168.5.200:6443

args=(
	--with-secrets
	secrets.yaml
	--install-disk
	/dev/mmcblk0
	"$CLUSTER_NAME"
	"$CLUSTER_ENDPOINT"
	--force
	--kubernetes-version 1.31.2
	)

helm repo add cilium https://helm.cilium.io/
# the format here is a ilttle tricky
# `helm template` generates multiple yaml docs
# and I can't figure out how to use `yq` over them as an array;
# jq has `--slurp` that helps with this.  So we will take the output as a string
# into jq, build the correct structure, and then use `yq` to reconvert back to YAML
# NOTE: `helm template` also spits out comments, and that broke _all the things_, so we also grep -v those
helm template cilium cilium/cilium \
	--version 1.16.5 \
	--namespace cilium \
    --set ipam.mode=kubernetes \
    --set kubeProxyReplacement=true \
    --set securityContext.capabilities.ciliumAgent="{CHOWN,KILL,NET_ADMIN,NET_RAW,IPC_LOCK,SYS_ADMIN,SYS_RESOURCE,DAC_OVERRIDE,FOWNER,SETGID,SETUID}" \
    --set securityContext.capabilities.cleanCiliumState="{NET_ADMIN,SYS_ADMIN,SYS_RESOURCE}" \
    --set cgroup.autoMount.enabled=false \
    --set cgroup.hostRoot=/sys/fs/cgroup \
    --set k8sServiceHost=localhost \
	--set k8sServicePort=7445 | grep -Ev '^#' | jq -rRs '{cluster: {inlineManifests: [{name: "cilium-zinstall", "contents": .}]}}' | yq -P > ./patches/controlplane/ciliumz-after-ns.yaml

for f in ./patches/controlplane/*.yaml; do
	args+=("--config-patch-control-plane" "@$f")
done

for f in ./patches/*.yaml; do
	args+=("--config-patch" "@$f")
done

talosctl gen config "${args[@]}"
