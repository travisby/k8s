terraform {
  required_version = "~> 1.0"

  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = "~> 4.0"
    }
  }
}

resource "vault_auth_backend" "k8s" {
  type = "kubernetes"
}

resource "vault_kubernetes_auth_backend_config" "this" {
  kubernetes_host = "https://kubernetes.default.svc.cluster.local"

  depends_on = [vault_auth_backend.k8s]
}

resource "vault_kubernetes_auth_backend_role" "this" {
  role_name                        = "external-secrets-operator"
  bound_service_account_names      = ["external-secrets-operator"]
  bound_service_account_namespaces = ["external-secrets"]

  token_policies = [vault_policy.external-secrets.name]

  depends_on = [vault_auth_backend.k8s]
}

resource "vault_mount" "external-secrets" {
  type = "kv-v2"
  path = "external-secrets"
}

data "vault_policy_document" "external-secrets" {
  rule {
    path         = "${vault_mount.external-secrets.path}/*"
    capabilities = ["read"]
  }
}

resource "vault_policy" "external-secrets" {
  name   = "external-secrets"
  policy = data.vault_policy_document.external-secrets.hcl
}
