# gitops for my personal cluster utilizing argocd

The directory structure is:
	* apps/{{argo project name}}/{{kubernetes namespace}}/{{application name}}/{helm chart or manifests here} -> picked up by an argo ApplicationSet and syncs on demand
	* /manually-applied-manifests/ -> manifests to be applied in the target cluster by hand for bootstrapping, secrets purposes, etc.
	* butane -> CoreOS Butane files for the k8s nodes hosted in an ovirt cluster
